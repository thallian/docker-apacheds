[ApacheDS](http://directory.apache.org/apacheds/) ldap server which provisions
tls certificates through [Let's Encrypt](https://letsencrypt.org/) with
[lego](https://github.com/xenolf/lego). 

Take a look at the [base image](https://gitlab.com/thallian/docker-confd-env/tree/lego)
for the certificate configuration.

# Volumes
- `/home/apacheds/instances/default/conf`
- `/home/apacheds/instances/default/partitions`
- `/home/apacheds/.lego`: certificates directory

# Ports
- 10389
- 10636
