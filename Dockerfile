FROM registry.gitlab.com/thallian/docker-confd-env:lego

ENV VERSION 2.0.0-M24
ENV CERT_HOME /home/apacheds
ENV CERT_USER apacheds

RUN addgroup -g 2222 apacheds
RUN adduser -h /home/apacheds -S -D -u 2222 -G apacheds apacheds

RUN apk add --no-cache openjdk8

RUN wget -qO- http://mirror.switch.ch/mirror/apache/dist//directory/apacheds/dist/${VERSION}/apacheds-${VERSION}.tar.gz | tar xz -C /home/apacheds --strip 1

ADD /rootfs /

VOLUME /home/apacheds/instances/default/conf /home/apacheds/instances/default/partitions /home/apacheds/.lego

EXPOSE 10389 10636
